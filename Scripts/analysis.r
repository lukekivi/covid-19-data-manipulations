## Created by Lucas Kivi
## Analysis of COVID time-series in midwestern urban counties with "stay-at-home order status
## as a dependent variable. April 6 - May 1
## Last edit 11/3/2020

library(tidyr)
library(dplyr)
library(ggplot2)

df <- read.csv("Dataset/urbanMidwest_stayathome_data.csv", stringsAsFactors = FALSE, as.is = TRUE)

# Remove classification and s because it maintains a constant value "Urban"
df <- df[,c(-4)]

## By state

# No need for cumulative cases
sDf <- df[,-5]

stateDf <- sDf %>%
  group_by(State, Date, Stay.At.Home) %>%
  summarise("Total New Cases" = sum(`New.Cases`))


## Plots

# False
plotFalse <- stateDf %>% filter(Stay.At.Home == FALSE)

plotFalse <- plotFalse[,-3]
  
plot <- ggplot(plotFalse,aes(x = Date, y = `Total New Cases`, color = State, group = State)) +
  geom_line(size = 1)
  
plot  

# True
plotTrue <- stateDf %>% filter(Stay.At.Home == TRUE)

plot2 <- ggplot(plotTrue,aes(x = Date, y = `Total New Cases`, color = State, group = State)) +
  geom_line(size = 1) +
  scale_x_date(b)

plot2  
  

